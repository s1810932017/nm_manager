from django.test import TestCase
from moi.models import NsInfo
from moi.models import SST
from moi.models import SNSSAIList
from moi.models import PLMNIdList
from moi.models import PerfRequirements
from moi.models import SliceProfileList
from moi.models import NetworkSliceSubnet

# Create your tests here.
# nsInfo1 = NsInfo(nsInstanceId=1, description="test1")
# nsInfo2 = NsInfo(nsInstanceId=2, description="test2")
# nsInfo3 = NsInfo(nsInstanceId=3, description="test3")
# nsInfo1.save()
# nsInfo2.save()
# nsInfo3.save()
# object1 = NetworkSliceSubnet(1, "1", "1", "ENABLED", "LOCKED", 1)
# object2 = NetworkSliceSubnet(2, "2", "2", "DISABLED", "UNLOCKED", 2)
# object3 = NetworkSliceSubnet(3, "3", "3", "ENABLED", "UNLOCKED", 3)
# object1.save()
# object2.save()
# object3.save()
# object1.NetworkSliceSubnet_sliceProfileList.create()
# object2.NetworkSliceSubnet_sliceProfileList.create()
# object3.NetworkSliceSubnet_sliceProfileList.create()
# SliceProfileList.objects.get(id=1).SliceProfileList_sNSSAIListId.create(id=1)
# SliceProfileList.objects.get(id=1).SliceProfileList_pLMNIdList.create(mcc=460, mnc=99)
# SliceProfileList.objects.get(id=1).SliceProfileList_perfReqId.create()
# SliceProfileList.objects.get(id=2).SliceProfileList_sNSSAIListId.create(id=2)
# SliceProfileList.objects.get(id=2).SliceProfileList_pLMNIdList.create(mcc=460, mnc=99)
# SliceProfileList.objects.get(id=2).SliceProfileList_perfReqId.create()
# SliceProfileList.objects.get(id=3).SliceProfileList_sNSSAIListId.create(id=3)
# SliceProfileList.objects.get(id=3).SliceProfileList_pLMNIdList.create(mcc=460, mnc=99)
# SliceProfileList.objects.get(id=3).SliceProfileList_perfReqId.create()
# SNSSAIList.objects.get(id=1).SNSSAIList_sST.create()
# SNSSAIList.objects.get(id=2).SNSSAIList_sST.create()
# SNSSAIList.objects.get(id=3).SNSSAIList_sST.create()

sst1 = SST(1, "eMBB",
           "Slice suitable for the handling of 5G enhanced Mobile Broadband.")
sst2 = SST(2, "URLLC",
           "Slice suitable for the handling of ultra- reliable low latency communications.")
sst3 = SST(3, "MIOT",
           "Slice suitable for the handling of massive IoT.")
sst1.save()
sst2.save()
sst3.save()
SNSSAIList1 = SNSSAIList(sD="1")
SNSSAIList2 = SNSSAIList(sD="FFFFFF")
SNSSAIList3 = SNSSAIList(sD="FFFFFF")
SNSSAIList1.save()
SNSSAIList2.save()
SNSSAIList3.save()
SNSSAIList1.sST.add(sst1)
SNSSAIList2.sST.add(sst2)
SNSSAIList3.sST.add(sst3)
PLMNIdList1 = PLMNIdList('46601', '466', '01', "Telecom01")
PLMNIdList2 = PLMNIdList('46611', '466', '11', "Telecom11")
PLMNIdList3 = PLMNIdList('46697', '466', '97', "Telecom97")
PLMNIdList1.save()
PLMNIdList2.save()
PLMNIdList3.save()
PerfRequirements1 = PerfRequirements(scenario="Urban macro",
                                     experiencedDataRateDL="50 Mbps",
                                     experiencedDataRateUL="25 Mbps",
                                     areaTrafficCapacityDL="100 Gbps/km2",
                                     areaTrafficCapacityUL="50 Gbps/km2",
                                     overallUserDensity="10 000/km2",
                                     activityFactor="20%",
                                     ueSpeed="Pedestrians and users in vehicles (up to 120 km/h)",
                                     coverage="Full network")
PerfRequirements2 = PerfRequirements(scenario="Rural macro",
                                     experiencedDataRateDL="50 Mbps",
                                     experiencedDataRateUL="25 Mbps",
                                     areaTrafficCapacityDL="1 Gbps/km2",
                                     areaTrafficCapacityUL="500 Gbps/km2",
                                     overallUserDensity="100/km2",
                                     activityFactor="20%",
                                     ueSpeed="Pedestrians and users in vehicles (up to 120 km/h)",
                                     coverage="Full network")
PerfRequirements3 = PerfRequirements(scenario="Indoor hotspot", experiencedDataRateDL="1 Gbps",
                                     experiencedDataRateUL="500 Mbps",
                                     areaTrafficCapacityDL="15 Tbps/km2",
                                     areaTrafficCapacityUL="2 Tbps/km2",
                                     overallUserDensity=F"250 000/km2",
                                     activityFactor="30%", ueSpeed="Pedestrians",
                                     coverage="Full Office and residential")
PerfRequirements1.save()
PerfRequirements2.save()
PerfRequirements3.save()
SliceProfileList1 = SliceProfileList()
SliceProfileList2 = SliceProfileList()
SliceProfileList3 = SliceProfileList()
SliceProfileList1.save()
SliceProfileList2.save()
SliceProfileList3.save()
SliceProfileList1.perfReqId.add(PerfRequirements1)
SliceProfileList2.perfReqId.add(PerfRequirements2)
SliceProfileList3.perfReqId.add(PerfRequirements3)
SliceProfileList1.sNSSAIListId.add(SNSSAIList1)
SliceProfileList2.sNSSAIListId.add(SNSSAIList2)
SliceProfileList3.sNSSAIListId.add(SNSSAIList3)
SliceProfileList1.pLMNIdList.add(PLMNIdList1)
SliceProfileList2.pLMNIdList.add(PLMNIdList2)
SliceProfileList3.pLMNIdList.add(PLMNIdList3)
NsInfo1 = NsInfo()
NsInfo2 = NsInfo()
NsInfo3 = NsInfo()
NsInfo1.save()
NsInfo2.save()
NsInfo3.save()
NetworkSliceSubnet1 = NetworkSliceSubnet(nssiId="18174e981dd540bfa200bcb23ff383a7",
                                         mFIdList=["916c3382abad47668824c8e56851923b"],
                                         administrativeState="LOCKED",
                                         operationalState="ENABLED", nsInfo=NsInfo1)
NetworkSliceSubnet2 = NetworkSliceSubnet(nssiId="930f60fb0d2f4c81b1432c52ede4ee70",
                                         mFIdList=["a32fbfc246044687924979a982aecf96"],
                                         administrativeState="UNLOCKED",
                                         operationalState="DISABLED", nsInfo=NsInfo2)
NetworkSliceSubnet3 = NetworkSliceSubnet(nssiId="bf737b0a56594a8ba445f4ed8543c95c",
                                         mFIdList={"d08fa22ef8a3459a97552fda0da53194",
                                                   "a32fbfc246044687924979a982aecf96",
                                                   "18174e981dd540bfa200bcb23ff383a7"},
                                         administrativeState="UNLOCKED",
                                         operationalState="ENABLED", nsInfo=NsInfo3)
NetworkSliceSubnet1.save()
NetworkSliceSubnet2.save()
NetworkSliceSubnet3.save()
NetworkSliceSubnet1.constituentNSSIIdList.add('930f60fb0d2f4c81b1432c52ede4ee70')
NetworkSliceSubnet2.constituentNSSIIdList.add('bf737b0a56594a8ba445f4ed8543c95c')
NetworkSliceSubnet3.constituentNSSIIdList.add('18174e981dd540bfa200bcb23ff383a7')
NetworkSliceSubnet1.sliceProfileList.add(SliceProfileList1)
NetworkSliceSubnet2.sliceProfileList.add(SliceProfileList2)
NetworkSliceSubnet3.sliceProfileList.add(SliceProfileList3)
