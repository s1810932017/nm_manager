from enum import Enum


class Scope(Enum):
    BASE_ONLY = 'BASE_ONLY'
    BASE_NTH_LEVEL = 'BASE_NTH_LEVEL'
    BASE_SUBTREE = 'BASE_SUBTREE'
    BASE_ALL = 'BASE_ALL'

    @classmethod
    def has_value(cls, value):
        return any(value == item.value for item in cls)
