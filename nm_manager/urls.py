"""nm_manager URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
from rest_framework.authtoken.views import obtain_auth_token
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

schema_view = get_schema_view(
    openapi.Info(
        title="free5gmano NM manager API",
        default_version='v1',
        description="ETSI TS 128 541 V15.5.0 - 5G Management and orchestration;\
        5G Network Resource Model (NRM); Stage 2 and stage 3 IMPORTANT: In case of \
        Please note that this file might be not aligned to the current version of \
        discrepancies the published ETSI Group Specification takes precedence. refer \
        https://www.etsi.org/deliver/etsi_ts/128500_128599/128541/15.05.00_60/ts_128541v150500p.pdf\
        ",
        terms_of_service="https://github.com/free5gmano/free5gmano",
        contact=openapi.Contact(email="free5gmano@gmail.com"),
        license=openapi.License(name="BSD License"),
    ),
    public=False,
    permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0),
        name='schema-json'),
    url(r'^swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    url(r'^redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
    path('', include('nssmf.urls')),
    path('', include('moi.urls')),
    path('', include('fault_supervision.urls')),
    path('api-token-auth/', obtain_auth_token, name='api_token_auth'),
]
