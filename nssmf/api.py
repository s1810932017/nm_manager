import requests
import json
import base64
import nm_manager.settings


nfvo_host = nm_manager.settings.NFVO_HOST
kafka_host = nm_manager.settings.KAFKA_HOST


class MOI:
    def __init__(self, reference_object: str, attribute_list_in: dict, _type: str):
        self.data = dict()
        self.data['referenceObjectInstance'] = reference_object
        self.data['attributeListIn'] = attribute_list_in
        self.url = "http://localhost:8000/ObjectManagement/{}/".format(_type) + "{}/"
        self.headers = {'Content-type': 'application/json'}

    def create(self, slice_id):
        # CreateMOI
        combine_url = self.url.format(slice_id)
        print(combine_url)
        print(self.data)
        response = requests.put(combine_url, data=json.dumps(self.data), headers=self.headers)
        return response

    def get(self, slice_id, scope):
        # Get MOI, uuid of slice id can't add dash
        # '["BASE_ONLY", 1]'
        combine_url = self.url.format(slice_id)

        payload = {'scope': str(scope),
                   'filter': "nssiId='{}'".format(slice_id)}
        print(payload)
        response = requests.get(combine_url, params=payload, headers=self.headers)
        return response

    def modify(self, slice_id, scope, config):
        # Modify MOI
        combine_url = self.url.format(slice_id)
        print(combine_url)
        data = {
            "modificationList": [
                [
                    config['attribute']
                ],
                [
                    config['value']
                ],
                "REPLACE"
            ]
        }
        print(data)
        payload = {'scope': str(scope)}
        response = requests.patch(combine_url, data=json.dumps(data), params=payload, headers=self.headers)
        return response

    def delete(self, slice_id):
        # Delete MOI
        combine_url = self.url.format(slice_id)
        response = requests.delete(combine_url, headers=self.headers)
        return response


class VNFPackage:
    def __init__(self):
        self.url = nfvo_host + "/vnfpkgm/v1/"

    def query(self):
        # Query VNF packages information.
        url = self.url + "vnf_packages/"
        headers = {'Content-type': 'application/json'}
        response = requests.get(url, headers=headers)
        return response

    def create(self, moi_config):
        # Create a new individual VNF package resource.
        url = self.url + "vnf_packages/"
        headers = {'Content-type': 'application/json'}
        data = {
            "userDefinedData": {
                "data": str(moi_config)
            }
        }
        response = requests.post(url, data=json.dumps(data), headers=headers)
        return response

    def read(self, vnf_pkg_id):
        # Read information about an individual VNF package.
        url = self.url + "vnf_packages/{}/".format(vnf_pkg_id)
        headers = {'Content-type': 'application/json'}
        response = requests.get(url, headers=headers)
        return response

    def delete(self, vnf_pkg_id):
        # Delete an individual VNF package.
        url = self.url + "vnf_packages/{}/".format(vnf_pkg_id)
        headers = {'Content-type': 'application/json'}
        response = requests.delete(url, headers=headers)
        return response

    def update(self, vnf_pkg_id):
        # Update information about an individual VNF package.
        response = self.url
        return response

    def read_on_board(self):
        # Read VNFD of an on-boarded VNF package.
        response = self.url
        return response

    def get_on_board(self):
        # Fetch an on-boarded VNF package.
        response = self.url
        return response

    def upload(self, vnf_pkg_id, vnf_pkg_path, pkg):
        # Upload a VNF package by providing the content of the VNF package.
        url = self.url + "vnf_packages/{}/package_content/".format(vnf_pkg_id)
        path = vnf_pkg_path
        files = {'file': (pkg, open(path, 'rb').read(),
                          'application/zip', {'Expires': '0'})}
        headers = {
            'Accept': "application/json,application/zip",
            'accept-encoding': "gzip, deflate"
        }
        response = requests.put(url, files=files, headers=headers)
        return response

    def upload_uri(self):
        # Upload a VNF package by providing the address information of the VNF package.
        response = self.url
        return response

    def get_uri(self):
        # Fetch individual VNF package artifact.
        response = self.url
        return response

    def subscribe(self, vnf_pkg_id, callback_uri):
        # Subscribe to notifications related to on-boarding and/or changes of VNF packages.
        url = self.url + 'subscriptions/'
        headers = {'Content-type': 'application/json'}
        data = {
            "filter": {
                "vnfPkgId": [vnf_pkg_id]
            },
            "callbackUri": callback_uri
        }
        response = requests.post(url, data=json.dumps(data), headers=headers)
        return response

    def query_subscribe(self):
        # Query multiple subscriptions.
        response = self.url
        return response

    def read_individual_subscribe(self):
        # Read an individual subscription resource.
        response = self.url
        return response

    def delete_subscribe(self):
        # Terminate a subscription.
        response = self.url
        return response


class NSDescriptors:
    def __init__(self):
        self.url = nfvo_host + "/nsd/v1/"

    def create(self):
        # Create a new NS descriptor resource.
        url = self.url + "ns_descriptors/"
        data = {
            "userDefinedData": {}
        }
        headers = {'Content-type': 'application/json'}
        response = requests.post(url, data=json.dumps(data), headers=headers)
        return response

    def query(self):
        # Query information about multiple NS descriptor resources.
        url = self.url + "ns_descriptors/"
        headers = {'Content-type': 'application/json'}
        response = requests.get(url, headers=headers)
        return response

    def read(self, ns_descriptor_id):
        # Read information about an individual NS descriptor resource.
        url = self.url + "ns_descriptors/" + "{}/".format(ns_descriptor_id)
        headers = {'Content-type': 'application/json'}
        response = requests.get(url, headers=headers)
        return response

    def modify(self):
        # Modify the operational state and/or
        # the user defined data of an individual NS descriptor resource.
        response = self.url
        return response

    def delete(self):
        # Delete an individual NS descriptor resource.
        response = self.url
        return response

    def fetch(self):
        # Fetch the content of a NSD.
        response = self.url
        return response

    def upload(self, ns_descriptor_id, ns_descriptor_path, pkg):
        # Upload the content of a NSD.
        url = self.url + "ns_descriptors/" + "{}/nsd_content/".format(ns_descriptor_id)
        path = ns_descriptor_path
        files = {'file': (pkg, open(path, 'rb').read(),
                          'application/zip', {'Expires': '0'})}
        headers = {
            'Accept': "application/json,application/zip",
            'accept-encoding': "gzip, deflate"
        }
        response = requests.put(url, files=files, headers=headers)
        return response

    def subscribe(self, nsd_object_id, callback_uri):
        # Subscribe to NSD and PNFD change notifications.
        url = self.url + 'subscriptions/'
        headers = {'Content-type': 'application/json'}
        data = {
            "filter": {
                "nsdInfoId": [nsd_object_id]
            },
            "callbackUri": callback_uri
        }
        response = requests.post(url, data=json.dumps(data), headers=headers)
        return response

    def query_subscribe(self):
        # Query multiple subscriptions.
        response = self.url
        return response

    def read_individual_subscribe(self):
        # Read an individual subscription resource.
        response = self.url
        return response

    def delete_subscribe(self):
        # Terminate a subscription.
        response = self.url
        return response


class NSInstances:
    def __init__(self):
        self.url = nfvo_host + "/nslcm/v1/"

    def create(self, nsdId):
        # Create a new NS instances resource.
        url = self.url + "ns_instances/"
        data = {
            "nsdId": nsdId,
            "nsName": "string",
            "nsDescription": "string"
        }
        headers = {'Content-type': 'application/json'}
        response = requests.post(url, data=json.dumps(data), headers=headers)
        return response

    def instance(self, ns_instance_id, vnf_instance_data):
        url = self.url + "ns_instances/{}/instantiate/".format(ns_instance_id)
        data = {"vnfInstanceData": vnf_instance_data}
        headers = {'Content-type': 'application/json'}
        response = requests.post(url, data=json.dumps(data), headers=headers)
        return response

    def subscribe(self, ns_instance_id, callback_uri):
        # Subscribe to NSD and PNFD change notifications.
        url = self.url + 'subscriptions/'
        headers = {'Content-type': 'application/json'}
        data = {
            "filter": {
                "nsInstanceSubscriptionFilter": {
                    "nsInstanceIds": [ns_instance_id]
                }
            },
            "callbackUri": callback_uri
        }
        response = requests.post(url, data=json.dumps(data), headers=headers)
        return response


class Kafka:
    def __init__(self):
        self.url = kafka_host + "/{}"

    def read_topic(self, topic=None):
        # Get a list of topics or info about one topic
        combine_url = self.url.format("topics/")
        if topic:
            combine_url += str(topic)
        header = {"Content-Type": "application/json"}
        response = requests.get(url=combine_url, headers=header)
        return response

    def producer(self, topic, data, partitions=None):
        # Produce a message with JSON data
        combine_url = self.url.format("topics/" + topic)
        if partitions:
            combine_url += "/partitions/{}".format(partitions)
        print(combine_url)
        header = {"Content-Type": "application/vnd.kafka.v1+json"}
        b64_data = base64.b64encode(str(data).encode())
        _data = {"records": [{"value": b64_data.decode()}]}
        response = requests.post(url=combine_url, json=_data, headers=header)
        return response

    def create_consumer(self, group: str, gid: str):
        # Create a consumer for JSON data, starting at the beginning of the topic's
        combine_url = self.url.format("consumers/" + group)
        data = {
            "id": gid,
            "format": "binary",  # "binary"
            "auto.offset.reset": "earliest",
            "auto.commit.enable": "false"
        }
        header = {"Content-Type": "application/vnd.kafka.v2+json"}
        response = requests.post(url=combine_url, json=data, headers=header)
        return response

    def delete_consumer(self, group: str, gid: str):
        # Create a consumer for JSON data, starting at the beginning of the topic's
        combine_url = self.url.format(
            "consumers/{}".format(group) +
            "/instances/{}".format(gid))
        header = {"Content-Type": "application/vnd.kafka.v2+json"}
        response = requests.delete(url=combine_url, headers=header)
        return response

    def subscribe(self, group: str, gid: str, topic: str):
        # Subscribe the consumer to a topic
        combine_url = self.url.format(
            "consumers/{}".format(group) +
            "/instances/{}".format(gid) +
            "/subscription")
        data = {"topics": [topic]}
        header = {"Content-Type": "application/vnd.kafka.v2+json"}
        response = requests.post(url=combine_url, json=data, headers=header)
        return response

    def desubscribe(self, group: str, gid: str):
        # Subscribe the consumer to a topic
        combine_url = self.url.format(
            "consumers/{}".format(group) +
            "/instances/{}".format(gid) +
            "/subscription")
        header = {"Content-Type": "application/vnd.kafka.v2+json"}
        response = requests.delete(url=combine_url, headers=header)
        return response

    def subscribe_record(self, group: str, gid: str):
        # The consume some data from a topic using the base URL in the first response.
        combine_url = self.url.format(
            "consumers/{}".format(group) +
            "/instances/{}".format(gid) +
            "/records")
        header = {"Content-Type": "application/vnd.kafka.json.v2+json"}
        response = requests.get(url=combine_url, headers=header)
        return response

    def subscribe_sequence(self, group: str, gid: str, topic: str, status: str):
        response = self.create_consumer(group, gid)
        # print(response.status_code)
        if response.status_code in (200, 409):
            response = self.subscribe(group, gid, topic)
            # print(response.status_code)
            if response.status_code == 204:
                while 1:
                    response = self.subscribe_record(group, gid)
                    # print(response.status_code)
                    print(base64.b64decode(response.json()[0]['value']).decode())
                    if response.json() and base64.b64decode(response.json()[0]['value']).decode() == status:
                        print(response.json())

                        break
                # return base64.b64decode(response.json()[0]['value']).decode()
                return 'success'
# a='eycxJzogJzInfQ=='  ->  base64 encoded
# base64.b64decode(a).decode()