from nssmf.models import NsInfo
from nssmf.models import PLMNIdList
from nssmf.models import PerfRequirements
from nssmf.models import SNSSAIList
from nssmf.models import SST
from nssmf.models import SliceProfileList
from nssmf.models import NetworkSliceSubnet

# from nssmf.serializers import NsInfoSerializer
# from nssmf.serializers import SSTSerializer
# from nssmf.serializers import SNSSAIListSerializer
# from nssmf.serializers import PLMNIdListSerializer
# from nssmf.serializers import PerfRequirementsSerializer
# from nssmf.serializers import SliceProfileListSerializer
# from nssmf.serializers import NetworkSliceSubnetSerializer

# import os
# import json

# module_dir = os.path.dirname(__file__)
# sliceNRM = os.path.join(module_dir, 'sliceNRM.json')
# with open(sliceNRM, 'r') as f:
#     data = json.loads(f.read())

sst1 = SST(1, "eMBB",
           "Slice suitable for the handling of 5G enhanced Mobile Broadband.")
sst2 = SST(2, "URLLC",
           "Slice suitable for the handling of ultra- reliable low latency communications.")
sst3 = SST(3, "MIOT",
           "Slice suitable for the handling of massive IoT.")
sst1.save()
sst2.save()
sst3.save()
SNSSAIList1 = SNSSAIList(sST=sst1, sD="1")
SNSSAIList2 = SNSSAIList(sST=sst2, sD="FFFFFF")
SNSSAIList3 = SNSSAIList(sST=sst3, sD="FFFFFF")
SNSSAIList1.save()
SNSSAIList2.save()
SNSSAIList3.save()
PLMNIdList1 = PLMNIdList('46601', '466', '01', "遠傳電信股份有限公司")
PLMNIdList2 = PLMNIdList('46611', '466', '11' "中華電信股份有限公司")
PLMNIdList3 = PLMNIdList('46697', '466', '97' "台灣大哥大股份有限公司")
PLMNIdList1.save()
PLMNIdList2.save()
PLMNIdList3.save()
PerfRequirements1 = PerfRequirements(scenario="Urban macro",
                                     experiencedDataRateDL="50 Mbps",
                                     experiencedDataRateUL="25 Mbps",
                                     areaTrafficCapacityDL="100 Gbps/km2",
                                     areaTrafficCapacityUL="50 Gbps/km2",
                                     overallUserDensity="10 000/km2",
                                     activityFactor="20%",
                                     ueSpeed="Pedestrians and users in vehicles (up to 120 km/h)",
                                     coverage="Full network")
PerfRequirements2 = PerfRequirements(scenario="Rural macro",
                                     experiencedDataRateDL="50 Mbps",
                                     experiencedDataRateUL="25 Mbps",
                                     areaTrafficCapacityDL="1 Gbps/km2",
                                     areaTrafficCapacityUL="500 Gbps/km2",
                                     overallUserDensity="100/km2",
                                     activityFactor="20%",
                                     ueSpeed="Pedestrians and users in vehicles (up to 120 km/h)",
                                     coverage="Full network")
PerfRequirements3 = PerfRequirements(scenario="Indoor hotspot", experiencedDataRateDL="1 Gbps",
                                     experiencedDataRateUL="500 Mbps",
                                     areaTrafficCapacityDL="15 Tbps/km2",
                                     areaTrafficCapacityUL="2 Tbps/km2",
                                     overallUserDensity=F"250 000/km2",
                                     activityFactor="30%", ueSpeed="Pedestrians",
                                     coverage="Full Office and residential")
PerfRequirements1.save()
PerfRequirements2.save()
PerfRequirements3.save()
SliceProfileList1 = SliceProfileList(SNSSAIList1, PerfRequirements1, PerfRequirements1)
SliceProfileList2 = SliceProfileList(SNSSAIList2, PerfRequirements2, PerfRequirements2)
SliceProfileList3 = SliceProfileList(SNSSAIList3, PerfRequirements3, PerfRequirements3)
SliceProfileList1.save()
SliceProfileList2.save()
SliceProfileList3.save()
NsInfo1 = NsInfo(description="test1", nsdId=1).save()
NsInfo2 = NsInfo(description="test2", nsdId=2).save()
NsInfo3 = NsInfo(description="test3", nsdId=3).save()
NsInfo1.save()
NsInfo2.save()
NsInfo3.save()
NetworkSliceSubnet(mFIdList="1", constituentNSSIIdList="1", administrativeState="ENABLED",
                   operationalState="LOCKED", nsInfo=NsInfo1,
                   SliceProfileList=SliceProfileList1).save()
NetworkSliceSubnet(mFIdList="2", constituentNSSIIdList="2", administrativeState="DISABLED",
                   operationalState="UNLOCKED", nsInfo=NsInfo2,
                   SliceProfileList=SliceProfileList2).save()
NetworkSliceSubnet(mFIdList="3", constituentNSSIIdList="3", administrativeState="ENABLED",
                   operationalState="UNLOCKED", nsInfo=NsInfo3,
                   SliceProfileList=SliceProfileList3).save()