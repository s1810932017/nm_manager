import zipfile
import platform
import shutil
import os
import io
from nm_manager import settings

__system__ = platform.system()
__folder__ = os.getcwd()


def decompression(buffer, tid, _type):
    response = []
    directory = os.path.join(__folder__, "nssmf", "template", _type, str(tid))
    with zipfile.ZipFile(io.BytesIO(buffer)) as zf:
        # printing all the contents of the zip file
        zf.printdir()
        # extracting all the files
        print('Extracting all the files now...')
        zf.extractall(path=directory)
        print('Done!')
        for i in zf.namelist():
            if i[-1] != '/':
                response.append(i)
    return response


def compression(dst_dir: str = 'template_example | template',
                _type: str = "VNF | NSD",
                tid: str = "<UUID_TYPE>",
                file_name: str = "<Any File>"):
    directory = os.path.join(__folder__, settings.MEDIA_ROOT,
                             dst_dir, _type, str(tid), str(file_name))
    os.chdir(directory)
    with zipfile.ZipFile(directory + '.zip', mode='w', compression=zipfile.ZIP_DEFLATED) as zf:
        for root, folders, files in os.walk('.'):
            for s_file in files:
                a_file = os.path.join(root, s_file)
                if dst_dir == 'template':
                    zf.write(a_file, arcname=os.path.join(file_name, a_file))
                else:
                    zf.write(a_file)
    os.chdir(__folder__)
    return directory + '.zip'

#
# def choice_compression(file_name):
#     src_directory = os.path.join(os.getcwd(), 'nssmf', 'csar')
#     dest_directory = os.path.join(src_directory, file_name[0])
#     os.chdir(src_directory)
#     with zipfile.ZipFile(dest_directory + '.zip', mode='w', compression=zipfile.ZIP_DEFLATED) as zf:
#         for _ in file_name:
#             if 'Nrm' in _:
#                 os.chdir(os.path.join(src_directory, 'NRM'))
#                 a_file = os.path.join('.', _ + ".json")
#                 zf.write(a_file)
#             else:
#                 for root, folders, files in os.walk(os.path.join('.', _)):
#                     for s_file in files:
#                         a_file = os.path.join(root, s_file)
#                         zf.write(a_file, arcname=os.path.join(a_file))
#     print(dest_directory)
#     os.chdir(__folder__)
#     return dest_directory + '.zip'


def del_directory(directory_name):
    directory = os.path.join(__folder__, str(directory_name))
    try:
        shutil.rmtree(directory)
    except OSError as e:
        return e
    else:
        return "success"


