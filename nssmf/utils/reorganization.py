import json
import yaml

# {'nfs_ip': '10.0.0.1', 'nfs_path': 'qwe'}
def combine(path, file, config):
    with open(path + file, 'r') as stream:
        content = yaml.load(stream, Loader=yaml.FullLoader)
        print(content)
        resource = content['topology_template']['node_templates']['resource']['properties']['resource_definition']
        data = json.loads(resource)
    combine_data = data['metadata']['labels']
    combine_data['nfs_ip'] = config['nfs_ip']
    combine_data['nfs_path'] = config['nfs_path']
    data['metadata']['labels'] = combine_data
    content['topology_template']['node_templates']['resource']['properties']['resource_definition'] =\
        '>'# json.load(data)# json.dumps(data)
    print(content)
    with open(path+'123.yaml', 'w') as f:
        # yaml.dump_all([content], f)#, default_flow_style=False)
        f.write(yaml.safe_dump({'1':'2'}))