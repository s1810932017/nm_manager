from django.apps import AppConfig


class FaultSupervisionConfig(AppConfig):
    name = 'fault_supervision'
